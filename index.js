(async function(){
  function getRandomNumber(start = 1, end = 10) {
    //works when both start and end are >=1
    return (parseInt(Math.random() * end) % (end - start + 1)) + start;
  }

  console.log('KRL');
  async function promiseThatResolvesRandomlyAfterRandomNumnberOfSecondsGenerator() {
      let randomNumberOfSeconds = getRandomNumber(2, 10);
      const funcao = async function() {
        let randomiseResolving = getRandomNumber(1, 10);
        if (randomiseResolving > 10) {
          return {
            RESOLVE:true,
            randomNumberOfSeconds: randomNumberOfSeconds,
            randomiseResolving: randomiseResolving
          };
        } else {
          return {
            REJECT:true,
            randomNumberOfSeconds: randomNumberOfSeconds,
            randomiseResolving: randomiseResolving
          };
        }
      };

      return setTimeout(await funcao, randomNumberOfSeconds * 1000);
  };

  async function promiseThatResolvesRandomlyAfterRandomNumnberOfSecondsGenerator2() {
    return new Promise(function(resolve, reject) {
          let randomNumberOfSeconds = getRandomNumber(2, 10);
          setTimeout(function() {
            let randomiseResolving = getRandomNumber(1, 10);
            if (randomiseResolving > 5) {
              resolve({
                randomNumberOfSeconds: randomNumberOfSeconds,
                randomiseResolving: randomiseResolving
              });
            } else {
              reject({
                randomNumberOfSeconds: randomNumberOfSeconds,
                randomiseResolving: randomiseResolving
              });
            }
          }, randomNumberOfSeconds * 1000);
        });
};


  async function testAsync() {
    for (var i = 0; i < 10; i++) {
      try {
        result1 = await promiseThatResolvesRandomlyAfterRandomNumnberOfSecondsGenerator();
        console.log("Result 1 ASYNC ", result1);
        result2 = await promiseThatResolvesRandomlyAfterRandomNumnberOfSecondsGenerator();
        console.log("Result 2 ASYNC", result2);
        result3 = await promiseThatResolvesRandomlyAfterRandomNumnberOfSecondsGenerator();
        console.log("Result 3 ASYNC", result3);
      } catch (e) {
        console.log("Error ASYNC", e);
      } finally {
        console.log("This is done ASYNC");
      }
    }
  }
  
  testAsync();

  async function testAsync2() {
    for (var i = 0; i < 10; i++) {
      try {
        result1 = await promiseThatResolvesRandomlyAfterRandomNumnberOfSecondsGenerator2();
        console.log("Result 1 ASYNC JENNY", JSON.stringify(result1));
        result2 = await promiseThatResolvesRandomlyAfterRandomNumnberOfSecondsGenerator2();
        console.log("Result 2 ASYNC JENNY", result2);
        result3 = await promiseThatResolvesRandomlyAfterRandomNumnberOfSecondsGenerator2();
        console.log("Result 3 ASYNC JENNY", result3);
      } catch (e) {
        console.log("Error ASYNC JENNY", e);
      } finally {
        console.log("This is done ASYNC JENNY");
      }
    }
  }
  
  testAsync2();
  
})()
  
